import { useContext } from "react";
import { WindowSizeContext } from "../contexts/WindowSizeContext";


export function useWindowSize() {
    const value = useContext(WindowSizeContext);

    return value;
}