"use client"

import React, { createContext, useState, useEffect, ReactNode } from 'react';

interface WindowSize {
    width: number;
    height: number;
}

interface WindowSizeProviderProps {
    children: ReactNode;
}

const defaultWindowSize: WindowSize = {
    width: 0,
    height: 0,
};

// Criação do contexto com valor padrão
export const WindowSizeContext = createContext<WindowSize>(defaultWindowSize);


export const WindowSizeProvider: React.FC<WindowSizeProviderProps> = ({ children }) => {
    const [windowSize, setWindowSize] = useState<WindowSize>(defaultWindowSize);

    useEffect(() => {
        const handleResize = () => {
            setWindowSize({
                width: window.innerWidth,
                height: window.innerHeight,
            });
        };

        window.addEventListener('resize', handleResize);
        handleResize(); // Define o tamanho inicial

        return () => window.removeEventListener('resize', handleResize);
    }, []);

    return (
        <WindowSizeContext.Provider value={windowSize}>
            {children}
        </WindowSizeContext.Provider>
    );
};