import { useState } from "react"
import style from './Homepage.module.css'
import Image from 'next/image';
import Link from 'next/link';
import mainBanner from '../images/mainBanner.png';
import promocoesBanner from '../images/promocoesimage.png'
import acessoriosBanner from '../images/acessoriosimage.png'
import whteBanner from '../images/whteimage.png'
import { Modal } from "../components/Modal/Modal";
import BigButton from "../components/BigButton/BigButton";
import { ProductCard } from "@/app/components/ProductCard/ProductCard"
import { Product } from "@/app/models/Product"


export function Homepage(){
    const products: Product[] = [
        {
            id: 1,
            name: "teste",
            image: ["teste"],
            price: 500,
            oldPrice: 600,
            quantity: 3,
            p: 1,
            m: 1,
            g: 1,
            gg: 0,
            xgg: 0
        },
        {
            id: 2,
            name: "teste",
            image: ["teste"],
            price: 500,
            oldPrice: 600,
            quantity: 3,
            p: 1,
            m: 1,
            g: 0,
            gg: 0,
            xgg: 1
        },
        {
            id: 3,
            name: "teste",
            image: ["teste"],
            price: 500,
            oldPrice: 600,
            quantity: 3,
            p: 0,
            m: 1,
            g: 1,
            gg: 1,
            xgg: 0
        },
        {
            id: 4,
            name: "teste",
            image: ["teste"],
            price: 500,
            oldPrice: 600,
            quantity: 3,
            p: 0,
            m: 0,
            g: 0,
            gg: 1,
            xgg: 0
        },
    ]

    return(
        <div className={style.body}>
            <div className={style.titles}>
                <h5 className={style.title1}>Keith Haring & Blvck </h5>
                <h5 className={style.title2}>Uma Fusão de Arte e Moda</h5>
            </div>

            <BigButton buttonContent={"Conheça a Coleção"} color={"black"} shadow={true} enable={true}/>

            
            <Image
                src={mainBanner}
                alt="Descrição da imagem" 
                className={style.mainbanner}  
            />


            <h6 className={style.subtitles}>Conheça Também</h6>
            <div className={style.options}>

                <div className={style.column}>
                    <Link href='' className={style.imagelink}>
                        <Image
                            src={promocoesBanner}
                            alt="promocoes" 
                            style={{marginBottom: '15px'}}
                            className={style.hoverzoom}
                            id={style.promocoes}
                        />
                    </Link>

                    <Link href='' className={style.imagelink}>
                        <Image
                            src={acessoriosBanner}
                            alt="acessorios"  
                            className={style.hoverzoom} 
                            id={style.acessorios}
                        />
                    </Link>
                </div>

                <div className={style.column}>
                    <Link href='' className={style.imagelink}>                     
                        <Image
                            src={whteBanner}
                            alt="whte"   
                            width={455}
                            height={882}
                            className={style.hoverzoom}
                            id={style.white}
                        />
                    </Link>
                </div>

            </div>
            <h6 className={style.subtitles}>Produtos</h6>
            <div className={style.products}>
                {products.length < 1 ? (

                    <div className={style.notFound}>
                        <Image 
                            width={60}
                            height={60}
                            alt="search off icon"
                            src={"/images/searchOff.svg"}
                        />
                        <h2>Nenhum item encontrado</h2>
                        <p>Tente novamente para encontrar o que você precisa</p>
                    </div>

                    ) : null}

                <div className={style.productsTable}>
                    {
                        products.map(product => (
                            <a className={style.productA} href={`/produtos/${product.id}`}>
                                <ProductCard key={product.id} product={product} />
                            </a>
                        ))
                    }
                </div>
            </div>
            <BigButton buttonContent={"Ver Mais"} color={"black"} shadow={true} enable={true}/>
        </div>
    )
}