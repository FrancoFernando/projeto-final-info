"use client"

import Image from "next/image"
import styles from "./index.module.css"
import { ChangeEvent, MouseEvent, useEffect, useState } from "react"
import { Overlay } from "@/app/components/Overlay/Overlay"
import { useWindowSize } from "@/app/hooks/useWindowSize"
import { ProductCard } from "@/app/components/ProductCard/ProductCard"
import { Product } from "@/app/models/Product"
import { filterProducts, getAllProducts } from "@/app/services/product"


export default function Products() {

    const [filterDropwdown, setFilterDropdown] = useState<boolean>(false);
    const [filters, setFilters] = useState<string[]>([]);
    const [tags, setTags] = useState<string[]>([]);
    const [collections, setCollections] = useState<string[]>([]);
    const [products, setProducts] = useState<Product[]>([]);
    const [search, setSearch] = useState<string>("");

    const { width } = useWindowSize()

    function handleTagCheckBox(event: ChangeEvent<HTMLInputElement>) {
        const name = event.target.name;
        const index = filters.indexOf(name);

        if (index != -1) {
            const newFilters = filters.filter(item => item !== name);
            setFilters(newFilters);

            const newTags = tags.filter(item => item !== name);
            setTags(newTags);
        } else {
            setFilters([...filters, name]);
            setTags([...tags, name]);
        }
    }

    function handleCollectionCheckBox(event: ChangeEvent<HTMLInputElement>) {
        const name = event.target.name;
        const index = filters.indexOf(name);

        if (index != -1) {
            const newFilters = filters.filter(item => item !== name);
            setFilters(newFilters);

            const newCollections = collections.filter(item => item !== name);
            setCollections(newCollections);
        } else {
            setFilters([...filters, name]);
            setCollections([...collections, name]);
        }
    }

    function cleanFilters() {
        setFilters([]);
        setFilterDropdown(false);
    }

    function handleRemoveFilter(value: string) {
        const newFilters = filters.filter(item => item !== value);
        setFilters(newFilters);
    }

    function handleFilterDropdown(event: MouseEvent<HTMLDivElement>) {
        setFilterDropdown(!filterDropwdown);    
    }

    async function getProducts() {
        try {
            const products = await getAllProducts()
            console.log(products);
            setProducts(products);
        } catch (e) {
            console.log(e);
        }
    }

    async function handleSearch() {
    }

    async function handleFilters() {
        const result = filterProducts(tags, collections);

    } 

    useEffect(() => {
        getProducts()
    }, []);

    useEffect(() => {
        handleFilters()
    }, [filters])

    return (
        <main className={styles.productsPage}>
            <h1 className={styles.title}>Produtos</h1>
            <div className={styles.header}>
                <section className={styles.searchSection}>
                    <div className={styles.inputArea}>
                        <input type="text" placeholder="Pesquisar" value={search} onChange={(e) => setSearch(e.target.value)}/>
                        <Image
                            width={24}
                            height={24}
                            src={"/images/search.svg"}
                            alt="search icon"
                         />
                    </div>

                    <p>
                        {products.length} itens encontrados!
                    </p>

                    {
                        width < 950 ? (
                            <div className={styles.tags}>
                            {
                                filters.map((tag, index) => (
                                    <div key={index} className={styles.tag} >
                                        {tag}
                                        <div 
                                            onClick={() => handleRemoveFilter(tag)}
                                            className={tag}
                                        >
                                            <Image
                                                width={12}
                                                height={12}
                                                alt="close icon"
                                                src={"/images/close.svg"}
                                            />
                                        </div>
                                        
                                    </div>
                                ))
                            }
                        </div> 
                        ) : null
                    }

                </section>
                <section className={styles.filterSection}>

                    <div className={styles.filterButton} onClick={handleFilterDropdown}>
                        <p>
                            Filtros
                        </p>
                        <Image
                            width={24}
                            height={24}
                            src={"/images/filter.svg"}
                            alt="filter icon"
                         />
                    </div>
                    
                    {filterDropwdown ? (
                        <Overlay setOpen={setFilterDropdown}>
                            <div className={styles.filters}>
                                <section>
                                    <p>Categorias</p>

                                    <div className={styles.categories}>
                                        <div>
                                            <input type="checkbox" name="Camisas" checked={filters.includes("Camisas")} onChange={handleTagCheckBox}/> 
                                            Camisas
                                        </div>
                                        <div>
                                            <input type="checkbox" name="Acessórios" checked={filters.includes("Acessórios")} onChange={handleTagCheckBox}/> 
                                            Acessórios
                                        </div>
                                        <div>
                                            <input type="checkbox" name="Calças" checked={filters.includes("Calças")} onChange={handleTagCheckBox}/> 
                                            Calças
                                        </div>
                                        <div>
                                            <input type="checkbox" name="Casacos" checked={filters.includes("Casacos")} onChange={handleTagCheckBox}/> 
                                            Casacos
                                        </div>
                                        <div>
                                            <input type="checkbox" name="Masculino" checked={filters.includes("Masculino")} onChange={handleTagCheckBox}/> 
                                            Masculino
                                        </div>
                                        <div>
                                            <input type="checkbox" name="Feminino" checked={filters.includes("Feminino")} onChange={handleTagCheckBox}/> 
                                            Feminino
                                        </div>
                                    </div>
                                </section>

                                <section>
                                    <p>Coleções</p>

                                    <div className={styles.colections}>
                                        <div>
                                            <input type="checkbox" name="Keith Haring & Blvck" checked={filters.includes("Keith Haring & Blvck")} onChange={handleCollectionCheckBox}/> 
                                            Keith Haring & Blvck
                                        </div>
                                        <div>
                                            <input type="checkbox" name="Fortnite & Blvck" checked={filters.includes("Fortnite & Blvck")} onChange={handleCollectionCheckBox}/> 
                                            Fortnite & Blvck
                                        </div>
                                        <div>
                                            <input type="checkbox" name="Shorts" checked={filters.includes("Shorts")} onChange={handleCollectionCheckBox}/> 
                                            Shorts
                                        </div>
                                        <div>
                                            <input type="checkbox" name="White" checked={filters.includes("White")} onChange={handleCollectionCheckBox}/> 
                                            White
                                        </div>
                                    </div>
                                </section>

                                <p className={styles.clean} onClick={cleanFilters}>Limpar Filtros</p>
                            </div>
                        </Overlay>
                    ) : null}
                    
                    {
                        width >= 950 ? (
                            <div className={styles.tags}>
                            {
                                filters.map((tag, index) => (
                                    <div key={index} className={styles.tag} >
                                        {tag}
                                        <div 
                                            onClick={() => handleRemoveFilter(tag)}
                                            className={tag}
                                        >
                                            <Image
                                                width={12}
                                                height={12}
                                                alt="close icon"
                                                src={"/images/close.svg"}
                                            />
                                        </div>
                                        
                                    </div>
                                ))
                            }
                        </div> 
                        ) : null
                    }
                </section>
            </div>

            <div className={styles.products}>
                {products.length < 1 ? (

                    <div className={styles.notFound}>
                        <Image 
                            width={60}
                            height={60}
                            alt="search off icon"
                            src={"/images/searchOff.svg"}
                        />
                        <h2>Nenhum item encontrado</h2>
                        <p>Tente novamente para encontrar o que você precisa</p>
                    </div>

                    ) : null}

                <div className={styles.productsTable}>
                    {
                        products.map(product => (
                            <a key={product.id} className={styles.productA} href={`/produtos/${product.id}`}>
                                <ProductCard product={product} />
                            </a>
                        ))
                    }
                </div>
            </div>
        </main>
    )
}