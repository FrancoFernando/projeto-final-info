'use client'
import BigButton from "../../../components/BigButton/BigButton"
import { Input } from "../../../components/Input/Input"
import favorite_border from '../../../images/favorite_border.svg';
import arrow_forward from '../../../images/arrow_forward.svg';
import Image from 'next/image';
import { Product } from "@/app/models/Product"
import { ProductCard } from "@/app/components/ProductCard/ProductCard"

import style from "./page.module.css"
import { useEffect, useState } from "react";
import { getAllProducts, getProductById } from "@/app/services/product";

export default function ProductPage() { 

    const [product, setProduct] = useState<Product>();
    const [products, setProducts] = useState<Product[]>([]);

    const [size, setSize] = useState<string>("P");
    const [options, setOptions] = useState<string[]>([]);

    function handleSize(event: string) {
        
    }

    async function getProduct() {
        const splitUrl = window.location.href.split("/");
        const id = splitUrl[splitUrl.length - 1];

        const product = await getProductById(id);
        setProduct(product);
    }

    async function getProducts() {
        const products = await getAllProducts()

        setProducts(products)
    }

    useEffect(() => {
        getProduct();
        getProducts();
    }, []);

    useEffect(() => {

        if (product) {

            let quantity: number = 0
            let options: string[] = []
            if (size == "P") quantity = product.quantity_p
            if (size == "M") quantity = product.quantity_m
            if (size == "G") quantity = product.quantity_g
            if (size == "GG") quantity = product.quantity_gg
            if (size == "XGG") quantity = product.quantity_xgg
    
            for (let i = 1; i <= quantity; i++) {
                options.push(String(i));
            }

            setOptions(options);
        }
        
    }, [size])

    return (
        <main className={style.main}>
            <div className={style.center}>
                <div className={style.left}>

                    <div className={style.sideImagesDiv}>
                        <div className={style.sideImagesDivDiv}></div>
                        <div className={style.sideImagesDivDiv}></div>
                        <div className={style.sideImagesDivDiv}></div>
                    </div>

                    <div className={style.mainImageDiv}>
                        <div className={style.mainImageDivDiv}>

                        </div>
                    </div>

                </div>
                <div className={style.right}>

                    <h1 className={style.title}>{product?.name}</h1>

                    
                    <div className={style.row} style={{gap:16}}>
                        <p className={style.price}>R$ {product?.actualPrice.toFixed(2)}</p>
                        {product?.previousPrice ? (
                            <p className={style.oldPrice}>R$ {product?.previousPrice.toFixed(2)}</p>
                        ) : null}
                    </div>


                    <div className="">
                        <span className={style.quantity}>7 Itens em estoque</span>
                        <div className={style.row}>
                            <div className={style.block}>
                                <span>Quantidade</span>
                                <Input type="dropdown" options={options} placeholder="" onChange={handleSize} symbol="R$"/>
                            </div>

                            <div className={style.block}>
                                <span>Tipo</span>
                                <Input type="dropdown" options={["", "P", "M", "G", "GG", "XGG"]} placeholder="" onChange={(e) => setSize(e)} symbol="R$"/>
                            </div>
                        </div>
                    </div>

                    <div className={style.row} style={{gap:16}}>
                        <button className={style.black}>Adicionar ao Carrinho</button>
                        <div className={style.fav}> <Image src={favorite_border} alt="favorite_border"/> </div>
                    </div>

                    <div className={style.block}>
                        <span>Calcular Frete</span>
                        <div className={style.row} style={{gap:8}}>
                            <Input placeholder="Digite seu CEP" onChange={() => {}} width={246}/>
                            <div className={style.arrow}> <Image src={arrow_forward} alt="arrow_forward"/> </div>
                        </div>
                    </div>

                    <div className={style.block}>
                        <span>Descrição</span>
                        <div className={style.description}>
                            {product?.description}
                        </div>
                    </div>

                    <div className={style.block}>
                        <span>Tags</span>
                        <div className={style.tag}>
                            Sweater
                        </div>
                    </div>

                    <div className={style.collection}>
                        <span>Coleção</span>
                        <h2>Blvck Mohair</h2>
                        <div className={style.description}>
                            A coleção Blvck Mohair da Blvck é uma expressão de elegância atemporal e textura luxuosa. Cada peça é cuidadosamente confeccionada para alcançar um equilíbrio perfeito entre o estilo contemporâneo e o conforto. O Mohair, conhecido por sua suavidade e sedosidade, acrescenta uma sensação de luxo a suéteres, blazers, saias e muito mais. Esta coleção versátil e sofisticada é um convite para explorar o mundo da moda de alta qualidade e estilo atemporal.
                        </div>
                        <BigButton buttonContent="Conhecer Coleção" color="white" enable={true} shadow={true} />

                    </div>

                </div>
            </div>

            <h2 className={style.sectionTitle}>Produtos Semelhantes</h2>
            <div className={style.products}>
                {products.length < 1 ? (

                    <div className={style.notFound}>
                        <Image 
                            width={60}
                            height={60}
                            alt="search off icon"
                            src={"/images/searchOff.svg"}
                        />
                        <h2>Nenhum item encontrado</h2>
                        <p>Tente novamente para encontrar o que você precisa</p>
                    </div>

                    ) : null}

                <div className={style.productsTable}>
                    {
                        products.map(product => (
                            <ProductCard key={product.id} product={product} width={265}/>
                        ))
                    }
                </div>
            </div>
            <div className={style.buttonDiv}>
                <BigButton buttonContent="Ver Mais" color="black" enable={true} shadow={true} />
            </div>
            <h2 className={style.sectionTitle}>Outros Produtos</h2>
            <div className={style.products}>
                {products.length < 1 ? (

                    <div className={style.notFound}>
                        <Image 
                            width={60}
                            height={60}
                            alt="search off icon"
                            src={"/images/searchOff.svg"}
                        />
                        <h2>Nenhum item encontrado</h2>
                        <p>Tente novamente para encontrar o que você precisa</p>
                    </div>

                    ) : null}

                <div className={style.productsTable}>
                    {
                        products.map(product => (
                            <a className={style.productA} key={product.id} href={`/produtos/${product.id}`}>
                                <ProductCard product={product} width={265}/>
                            </a>
                        ))
                    }
                </div>
            </div>
            <div className={style.buttonDiv}>
                <BigButton buttonContent="Ver Mais" color="black" enable={true} shadow={true} />
            </div>

        </main>
    )
}