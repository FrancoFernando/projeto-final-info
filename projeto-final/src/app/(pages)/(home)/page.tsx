"use client"

import style from './Homepage.module.css'
import Image from 'next/image';
import Link from 'next/link';
import mainBanner from '../../images/mainBanner.png';
import promocoesBanner from '../../images/promocoesimage.png'
import acessoriosBanner from '../../images/acessoriosimage.png'
import whteBanner from '../../images/whteimage.png'
import BigButton from "../../components/BigButton/BigButton";
import { useRouter } from 'next/navigation';
import { ProductCard } from '@/app/components/ProductCard/ProductCard';
import { Product } from '@/app/models/Product';
import { useEffect, useState } from 'react';
import { getAllProducts } from '@/app/services/product';

export default function Homepage() {

    const [products, setProducts] = useState<Product[]>([]);
    const router = useRouter();

    async function getProduts() {
        try {
            const products = await getAllProducts();
            setProducts(products);
        } catch (e) {
            console.log(e);
        }
    }

    useEffect(() => {
        getProduts();
    }, [])

    return(
        <div className={style.body}>
            <div className={style.titles}>
                <h5 className={style.title1}>Keith Haring & Blvck </h5>
                <h5 className={style.title2}>Uma Fusão de Arte e Moda</h5>
            </div>

            <BigButton onClick={() => router.push("/produtos")} buttonContent={"Conheça a Coleção"} color={"black"} shadow={true} enable={true}/>
   
            <Image
                src={mainBanner}
                alt="Descrição da imagem" 
                className={style.mainbanner}

            />

            <h6 className={style.subtitles}>Conheça Também</h6>
            <div className={style.options}>

                <div className={style.column}>
                    <Link href='/produtos' className={style.imagelink}>
                        <Image
                            src={promocoesBanner}
                            alt="promocoes" 
                            style={{marginBottom: '15px'}}
                            className={style.hoverzoom}
                            id={style.promocoes}
                        />
                    </Link>

                    <Link href='/produtos' className={style.imagelink}>
                        <Image
                            src={acessoriosBanner}
                            alt="acessorios"  
                            className={style.hoverzoom} 
                            id={style.acessorios}
                        />
                    </Link>
                </div>

                <div className={style.column}>
                    <Link href='/produtos' className={style.imagelink}>                     
                        <Image
                            src={whteBanner}
                            alt="whte"   
                            width={455}
                            height={882}
                            className={style.hoverzoom}
                            id={style.white}
                        />
                    </Link>
                </div>

            </div>
            <h6 className={style.subtitles}>Produtos</h6>
            <div className={style.productsTable}>
                    {
                        products.map(product => (
                            <a key={product.id} className={style.productA} href={`/produtos/${product.id}`}>
                                <ProductCard product={product} />
                            </a>
                        ))
                    }
                </div>
            <BigButton onClick={() => router.push("/produtos")} buttonContent={"Ver Mais"} color={"black"} shadow={true} enable={true}/>
        </div>
    )
}
