"use client"

import Image from "next/image"
import styles from "./page.module.css"
import { ChangeEvent, MouseEvent, useState, useEffect } from "react"
import { Overlay } from "@/app/components/Overlay/Overlay"
import { useWindowSize } from "@/app/hooks/useWindowSize"
import { ProductCard } from "@/app/components/ProductCard/ProductCard"
import { Product } from "@/app/models/Product"
import { Modal } from "@/app/components/Modal/Modal"
import addWhite from "@/app/images/addWhite.svg"

import { putProduct, getAllProducts } from "@/app/services/product"



export default function Products() {

    const [filterDropwdown, setFilterDropdown] = useState<boolean>(false);
    const [filters, setFilters] = useState<string[]>([]);
    const { width } = useWindowSize()
    const [modalOpen, setModalOpen] = useState<boolean>(false);
    const [modalProduct, setModalProduct] = useState<Product>();
    const [modalTitle, setModalTitle] = useState<string>("");
    const [products, setProducts] = useState<Product[]>([]);

    // chamada api

    async function getProduts() {
        try {
            const products = await getAllProducts();
            setProducts(products);
        } catch (e) {
            console.log(e);
        }
    }

    useEffect(() => {
        getProduts();
    }, [])

    const product: Product[] = [
        {
            id: 1,
            name: "teste",
            images: [],
            actualPrice: 300,
            previousPrice: 600,
            quantity: 3,
            quantity_p: 1,
            quantity_m: 1,
            quantity_g: 1,
            quantity_gg: 0,
            quantity_xgg: 0,
            description: "teste",
            collections: ["1"],
        },
        {
            id: 2,
            name: "teste",
            images: [],
            actualPrice: 500,
            previousPrice: 600,
            quantity: 3,
            quantity_p: 1,
            quantity_m: 1,
            quantity_g: 0,
            quantity_gg: 0,
            quantity_xgg: 1
        },
        {
            id: 3,
            name: "teste",
            images: [],
            actualPrice: 500,
            previousPrice: 600,
            quantity: 3,
            quantity_p: 0,
            quantity_m: 1,
            quantity_g: 1,
            quantity_gg: 1,
            quantity_xgg: 0
        },
        {
            id: 4,
            name: "teste",
            images: [],
            actualPrice: 500,
            quantity: 1,
            quantity_p: 0,
            quantity_m: 0,
            quantity_g: 0,
            quantity_gg: 1,
            quantity_xgg: 0
        },
        {
            id: 5,
            name: "teste",
            images: [],
            actualPrice: 500,
            quantity: 1,
            quantity_p: 0,
            quantity_m: 0,
            quantity_g: 1,
            quantity_gg: 0,
            quantity_xgg: 0
        }
    ]


    function handleCheckBox(event: ChangeEvent<HTMLInputElement>) {
        const name = event.target.name;
        const index = filters.indexOf(name);

        if (index != -1) {
            const newFilters = filters.filter(item => item !== name);
            setFilters(newFilters);
        } else {
            setFilters([...filters, name]);
        }

    }

    function cleanFilters() {
        setFilters([]);
        setFilterDropdown(false);
    }

    function handleRemoveFilter(value: string) {
        const newFilters = filters.filter(item => item !== value);
        setFilters(newFilters);
    }

    function handleFilterDropdown(event: MouseEvent<HTMLDivElement>) {
        setFilterDropdown(!filterDropwdown);    
    }

    return (
        <main className={styles.productsPage}>
            {   modalOpen && 
                <Modal 
                    isOpen={modalOpen} 
                    title={modalTitle} 
                    product={modalProduct} 
                    close={ ()=> {
                        setModalOpen(false); 
                    }}
                    update={ () => { if(modalProduct) putProduct(modalProduct); }}
                />
            }
            <h1 className={styles.title}>Edição</h1>
            <div className={styles.header}>
                <div className={styles.first}>
                    
                    <div className={styles.black} onClick={() => {setModalProduct(undefined); setModalTitle("Criar Produto"); setModalOpen(true); }}>
                        <span>Criar Produto</span> 
                        <Image src={addWhite} alt="add"/> 
                    </div>

                    <div className={styles.sections}>
                        <section className={styles.searchSection}>
                            <div className={styles.inputArea}>
                                <input type="text" placeholder="Pesquisar"/>
                                <Image
                                    width={24}
                                    height={24}
                                    src={"/images/search.svg"}
                                    alt="search icon"
                                />
                            </div>
                        </section>
                        <section className={styles.filterSection}>

                            <div className={styles.filterButton} onClick={handleFilterDropdown}>
                                <p>
                                    Filtros
                                </p>
                                <Image
                                    width={24}
                                    height={24}
                                    src={"/images/filter.svg"}
                                    alt="filter icon"
                                />
                            </div>
                            
                            {filterDropwdown ? (
                                <Overlay setOpen={setFilterDropdown}>
                                    <div className={styles.filters}>
                                        <section>
                                            <p>Categorias</p>

                                            <div className={styles.categories}>
                                                <div>
                                                    <input type="checkbox" name="Camisas" checked={filters.includes("Camisas")} onChange={handleCheckBox}/> 
                                                    Camisas
                                                </div>
                                                <div>
                                                    <input type="checkbox" name="Acessórios" checked={filters.includes("Acessórios")} onChange={handleCheckBox}/> 
                                                    Acessórios
                                                </div>
                                                <div>
                                                    <input type="checkbox" name="Calças" checked={filters.includes("Calças")} onChange={handleCheckBox}/> 
                                                    Calças
                                                </div>
                                                <div>
                                                    <input type="checkbox" name="Casacos" checked={filters.includes("Casacos")} onChange={handleCheckBox}/> 
                                                    Casacos
                                                </div>
                                                <div>
                                                    <input type="checkbox" name="Masculino" checked={filters.includes("Masculino")} onChange={handleCheckBox}/> 
                                                    Masculino
                                                </div>
                                                <div>
                                                    <input type="checkbox" name="Feminino" checked={filters.includes("Feminino")} onChange={handleCheckBox}/> 
                                                    Feminino
                                                </div>
                                            </div>
                                        </section>

                                        <section>
                                            <p>Coleções</p>

                                            <div className={styles.colections}>
                                                <div>
                                                    <input type="checkbox" name="Keith Haring & Blvck" checked={filters.includes("Keith Haring & Blvck")} onChange={handleCheckBox}/> 
                                                    Keith Haring & Blvck
                                                </div>
                                                <div>
                                                    <input type="checkbox" name="Fortnite & Blvck" checked={filters.includes("Fortnite & Blvck")} onChange={handleCheckBox}/> 
                                                    Fortnite & Blvck
                                                </div>
                                                <div>
                                                    <input type="checkbox" name="Shorts" checked={filters.includes("Shorts")} onChange={handleCheckBox}/> 
                                                    Shorts
                                                </div>
                                                <div>
                                                    <input type="checkbox" name="White" checked={filters.includes("White")} onChange={handleCheckBox}/> 
                                                    White
                                                </div>
                                            </div>
                                        </section>

                                        <p className={styles.clean} onClick={cleanFilters}>Limpar Filtros</p>
                                    </div>
                                </Overlay>
                            ) : null}
                            
                            {
                                width >= 950 ? (
                                    <div className={styles.tags}>
                                    {
                                        filters.map((tag, index) => (
                                            <div key={index} className={styles.tag} >
                                                {tag}
                                                <div 
                                                    onClick={() => handleRemoveFilter(tag)}
                                                    className={tag}
                                                >
                                                    <Image
                                                        width={12}
                                                        height={12}
                                                        alt="close icon"
                                                        src={"/images/close.svg"}
                                                    />
                                                </div>
                                                
                                            </div>
                                        ))
                                    }
                                </div> 
                                ) : null
                            }
                        </section>
                    </div>
                </div>
                
                <div className={styles.found}>
                    <p>
                        {product.length} itens encontrados!
                    </p>

                    {
                        width < 950 ? (
                            <div className={styles.tags}>
                            {
                                filters.map((tag, index) => (
                                    <div key={index} className={styles.tag} >
                                        {tag}
                                        <div 
                                            onClick={() => handleRemoveFilter(tag)}
                                            className={tag}
                                        >
                                            <Image
                                                width={12}
                                                height={12}
                                                alt="close icon"
                                                src={"/images/close.svg"}
                                            />
                                        </div>
                                        
                                    </div>
                                ))
                            }
                        </div> 
                        ) : null
                    }
                </div>

            </div>

            <div className={styles.products}>
                {product.length < 1 ? (

                    <div className={styles.notFound}>
                        <Image 
                            width={60}
                            height={60}
                            alt="search off icon"
                            src={"/images/searchOff.svg"}
                        />
                        <h2>Nenhum item encontrado</h2>
                        <p>Tente novamente para encontrar o que você precisa</p>
                    </div>

                    ) : null}

                <div className={styles.productsTable}>
                    {
                        product.map(product => (
                            <ProductCard key={product.id} product={product} width={265} onClick={() => {setModalProduct(product); setModalTitle("Editar Produto"); setModalOpen(true); } }/>
                        ))
                    }
                </div>
            </div>
        </main>
    )
}