"use client"

import Image from "next/image"
import styles from "./index.module.css"
import { ChangeEvent, MouseEvent, SetStateAction, useEffect, useState } from "react"
import { Overlay } from "@/app/components/Overlay/Overlay"
import cart from "../../images/shopping_cart.svg"
import send from "../../images/arrow_forward.svg"
import products from "../produtos/page"
import favorite from "../../images/favorite_border.png";
import del from "../../images/delete.png"
import BigButton from "../../components/BigButton/BigButton";
import { ProductCard } from "@/app/components/ProductCard/ProductCard"
import { Product } from "@/app/models/Product"
import { getAllProducts } from "@/app/services/product"

export default function Cart(){

    const [tamanho, setTamanho] = useState('');
    const [quantidade, setQuantidade] = useState(1);
    const [products, setProducts] = useState<Product[]>([]);
    
    const opcoesTamanho = ['P', 'M', 'G', 'GG', 'XGG'];
    const opcoesQuantidade = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    
    const handleTamanhoChange = (event: { target: { value: SetStateAction<string> } }) => {
        setTamanho(event.target.value);
    };
    
    const handleQuantidadeChange = (event: { target: { value: any } }) => {
        setQuantidade(Number(event.target.value));
    };

    async function getProducts() {
        try {
            const products = await getAllProducts();
            console.log(products);
            setProducts(products);
        } catch (e) {
            console.log(e);
        }
    }

    useEffect(() => {
        getProducts()
    }, []);

    
    return (
        <main className={styles.carrinho}>
            <h1 className={styles.title}>Seu Carrinho</h1>
            {(productsSelectioned.length > 0) ? (
                <>
                    {products.map((product, index) => (
                        <div className={styles.menu} key={index}>
                            <div className={styles.products}>
                                <div className={styles.product}>
                                    <div>
                                        <Image
                                            src={cart}
                                            alt="cart"
                                            style={{ marginRight: '20px' }}
                                            width={170}
                                            height={170}
                                        />
                                    </div>
                                    <div className={styles.productsinfo}>
                                        <h4 className={styles.nameproduct}>{product.name}</h4>
                                        <p className={styles.priceproduct}>R$ {product.price.toFixed(2)}</p>
                                        <div className={styles.qtdeandtam}>
                                            <label className={styles.qtde}>
                                                <p className={styles.qtdetitle}>Quantidade:</p>
                                                <select
                                                    value={product.quantity}
                                                    onChange={(e) => handleQuantidadeChange(e)}
                                                    className={styles.qtdeoptions}
                                                >
                                                    {opcoesQuantidade.map((opcao, index) => (
                                                        <option key={index} value={quantidade}>
                                                            {opcao}
                                                        </option>
                                                    ))}
                                                </select>
                                            </label>
    
                                            <label className={styles.tam}>
                                                <p className={styles.tamtitle}>Tamanho:</p>
                                                <select
                                                    value={tamanho}
                                                    onChange={(e) => handleTamanhoChange(e)}
                                                    className={styles.tamoptions}
                                                >
                                                    {opcoesTamanho.map((opcao, index) => (
                                                        <option key={index} value={opcao}>
                                                            {opcao}
                                                        </option>
                                                    ))}
                                                </select>
                                            </label>
                                        </div>
                                    </div>
                                    <div className={styles.loveanddel}>
                                        <Image src={favorite} alt="fav" />
                                        <Image src={del} alt="del" style={{ marginLeft: "16px" }} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}
                    <div className={styles.menu}>
                        <div className={styles.payment}>
                            <div className={styles.buy}>
                                <p style={{ fontSize: '16px', fontWeight: 'bold' }}>Subtotal</p>
                                <p style={{ fontSize: '24px', fontWeight: 'bold' }}>R$ 3.906,00</p>
                                <div className={styles.buynow}>
                                    <BigButton buttonContent={"Comprar Agora"} color={"black"} shadow={false} enable={true} />
                                </div>
                                <div className={styles.calcfrete}>
                                    <p>Calcular frete</p>
                                    <div className={styles.sendcep}>
                                        <div className={styles.cep}></div>
                                        <div className={styles.sendcontainer}>
                                            <Image
                                                src={send}
                                                alt="send"
                                                className={styles.send}
                                            />
                                        </div>
                                    </div>
                                    <p style={{ fontSize: '10px' }}>Preço de frete para Salvador, Bahia: R$ 24,00</p>
                                </div>
                            </div>
                            <div className={styles.buycontinue}>
                                <BigButton buttonContent={"Continuar Comprando"} color={"white"} shadow={false} enable={true} />
                            </div>
                            <p style={{ color: '#6B6B6B' }}>Limpar carrinho</p>
                        </div>
                    </div>
                </>
            ) : (
                <div>
                    <Image
                        src={cart}
                        alt="cart"
                        style={{ marginBottom: '24px' }}
                    />
                    <p style={{ fontSize: '32px', fontWeight: 'bold', marginBottom: '24px' }}>Seu carrinho está vazio</p>
                    <p style={{ color: 'GrayText', marginBottom: '32px' }}>Adicione novos itens ao carrinho antes de prosseguir para o pagamento!</p>
                </div>
            )}
            <h1 className={styles.subtitles}>Vistos Recentemente</h1>
            <div className={styles.productsRecently}>
                {products.length < 1 ? (
                    <div className={styles.notFound}>
                        <Image
                            width={60}
                            height={60}
                            alt="search off icon"
                            src={"/images/searchOff.svg"}
                        />
                        <h2>Nenhum item encontrado</h2>
                        <p>Tente novamente para encontrar o que você precisa</p>
                    </div>
                ) : null}
                <div className={styles.productsTableRecently}>
                    {
                        products.map(product => (
                            <a className={styles.productA} key={product.id} href={`/produtos/${product.id}`}>
                                <ProductCard product={product} />
                            </a>
                        ))
                    }
                </div>
            </div>
            <h1 className={styles.subtitles}>Outros Produtos</h1>
            <div className={styles.products}>
                {products.length < 1 ? (
                    <div className={styles.notFound}>
                        <Image
                            width={60}
                            height={60}
                            alt="search off icon"
                            src={"/images/searchOff.svg"}
                        />
                        <h2>Nenhum item encontrado</h2>
                        <p>Tente novamente para encontrar o que você precisa</p>
                    </div>
                ) : null}
                <div className={styles.productsTable}>
                    {products.map(product => (
                        <a className={styles.productA} href={`/produtos/${product.id}`}>
                            <ProductCard key={product.id} product={product} />
                        </a>
                    ))}
                </div>
            </div>
        </main>
    )
    
}