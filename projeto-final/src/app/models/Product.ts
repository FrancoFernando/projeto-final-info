export type Product = {
    id: number,
    images: string[],
    name: string,
    previousPrice?: number,
    actualPrice: number,
    quantity: number,
    quantity_p: number,
    quantity_m: number,
    quantity_g: number,
    quantity_gg: number,
    quantity_xgg: number,
    description?: string,
    tags?: string[],
    collections?: string[],
}

export type CardProductType = {
    id: number,
    image: string,
    name: string,
    oldPrice?: number,
    price: number,
    quantity: number,
}