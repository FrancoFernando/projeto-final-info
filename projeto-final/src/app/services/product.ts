import { Product } from "../models/Product";
import { api } from "./axios";

export async function getAllProducts() {

    const res = await api.get("/products");
    const data = res.data;
    if (data.success) {
        return data.products;
    } else {
        throw new Error(data.error);
    }
}

export async function getProductById(id: string) {
    const res = await api.get(`/products/${id}`);
    const data = res.data;
    if (data.success) {
        return data.product;
    } else {
        throw new Error(data.error);
    }
}

export async function searchProduct(search: string) {
    const res = await api.get("/products");
    const data = res.data;
    if (data.success) {
        return data.products;
    } else {
        throw new Error(data.error);
    }
}

export async function filterProducts(tags: string[], collections: string[]) {

    let query = ""
    const joinTags = tags.join(",");
    const joinCollections = tags.join(",");

    if (tags.length > 0) {
        query += "?tags="
        query += joinTags;
        if (collections.length) {
            query += "&collections="
            query += joinCollections
        }
    } else if (collections.length >  0) {
        query += "?collections="
        query += joinCollections
    }


    console.log(query);
    // const res = await api.get("/products");
    // const data = res.data;
    // if (data.success) {
    //     return data.products;
    // } else {
    //     throw new Error(data.error);
    // }
}

export async function putProduct(prod: Product) {
    const res = await api.put("/products",{
        id: prod.id,
        name: prod.name,
        previousPrice: prod.previousPrice,
        actualPrice: prod.actualPrice,
        quantity: prod.quantity,
        quantity_p: prod.quantity_p,
        quantity_m: prod.quantity_m,
        quantity_g: prod.quantity_g,
        quantity_gg: prod.quantity_gg,
        quantity_xgg: prod.quantity_xgg,
        description: prod.description,
        tags: prod.tags,
        collections: prod.collections,
        images: JSON.stringify(prod.images),
    });

    const data = res.data;
    if (data.success) {
        let prod = {
            id: data.id,
            name: data.name,
            previousPrice: data.previousPrice,
            actualPrice: data.actualPrice,
            quantity: data.quantity,
            quantity_p: data.quantity_p,
            quantity_m: data.quantity_m,
            quantity_g: data.quantity_g,
            quantity_gg: data.quantity_gg,
            quantity_xgg: data.quantity_xgg,
            description: data.description,
            tags: data.tags,
            collections: data.collections,
            images: JSON.parse(data.images),
        }
        return data.products;
    } else {
        throw new Error(data.error);
    }
}


export async function postProduct(prod: Product) {
    const res = await api.post("/products",{
        id: prod.id,
        name: prod.name,
        previousPrice: prod.previousPrice,
        actualPrice: prod.actualPrice,
        quantity: prod.quantity,
        quantity_p: prod.quantity_p,
        quantity_m: prod.quantity_m,
        quantity_g: prod.quantity_g,
        quantity_gg: prod.quantity_gg,
        quantity_xgg: prod.quantity_xgg,
        description: prod.description,
        tags: prod.tags,
        collections: prod.collections,
        images: JSON.stringify(prod.images),
    });

    const data = res.data;
    if (data.success) {
        return data.products;
    } else {
        throw new Error(data.error);
    }
}