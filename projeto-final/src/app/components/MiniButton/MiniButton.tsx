'use client';

import React from 'react';
import styles from './MiniButton.module.css';

interface MiniButtonPropsType {
  buttonContent: string;
  color: 'black' | 'white';
  enable: boolean;
  onClick?: () => void;
}

export default function MiniButton(MiniButtonProps: MiniButtonPropsType) {
  const buttonClasses = `${styles.MiniButton} ${MiniButtonProps.color === 'black' ? styles.BlackMiniButton : styles.WhiteMiniButton}`;
  const buttonStyles = {
    cursor: MiniButtonProps.enable ? 'pointer' : 'not-allowed', // Altera o cursor com base na propriedade enable
  };

  // Cria um objeto separado para propriedades condicionais
  const conditionalStyles: React.CSSProperties = {};

  if (!MiniButtonProps.enable) {
    conditionalStyles['pointerEvents'] = 'none';
    conditionalStyles['opacity'] = '0.5';
  }

  // Mescla os objetos buttonStyles e conditionalStyles
  const mergedStyles = { ...buttonStyles, ...conditionalStyles };

  return (
    <>
      <button
      className={buttonClasses}
      style={mergedStyles}
      disabled={!MiniButtonProps.enable}
      onClick={MiniButtonProps.onClick}
      >
        {MiniButtonProps.buttonContent}
      </button>
    </>
  );
}
