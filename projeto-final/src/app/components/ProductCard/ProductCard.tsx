import { Product } from "@/app/models/Product"
import styles from "./productCard.module.css"

type ProductCardProps = {
    product: Product,
    width?: number,
    height?: number,
    onClick?: () => void
}

export function ProductCard({product, width, height, onClick}: ProductCardProps) {


    const quantity = product.quantity_g + product.quantity_gg + product.quantity_m + product.quantity_p + product.quantity_xgg


    return (
        <div className={styles.productCard} onClick={onClick} 
            style={ {   width: (width ? width : "360") + "px", 
                        height: (height ? height : "476") + "px" 
                    }}
        > 
            <div className={styles.image}
                style={ {   
                    width: (width ? width : "360") + "px", 
                    height: (height ? height : "360") + "px" 
                }}
            >

            </div>

            <div className={styles.info}>
                <h5 className={styles.name}>{product.name}</h5>
                <div className={styles.prices}>
                    <p className={styles.price}>R$ {product.actualPrice.toFixed(2)}</p>
                    {product.previousPrice ? (
                        <p className={styles.oldPrice}>R$ {product.previousPrice.toFixed(2)}</p>
                    ) : null}
                </div>
                {quantity > 1 ? (
                    <p className={styles.quantity}>{quantity} itens em estoque</p>
                ) : null}
            </div>
        </div>
    )
}