"use client"

import React, { useState } from 'react';
import styles from "./dropDownWeb.module.css"
import Arrow from './imgs/arrow.svg';
import Image from 'next/image';
interface DropDownWebProps {
  values: string[];
}

function TamanhoDropdown(props: DropDownWebProps) {
  const [showDropdown, setShowDropdown] = useState(false);
  const [selectedSize, setSelectedSize] = useState(props.values[0] || '');

  const toggleDropdown = () => {
    setShowDropdown(!showDropdown);
  };

  const handleSizeSelection = (value: any) => {
    setSelectedSize(value);
    setShowDropdown(false);
  };

  return (
    <div className={styles.dropdownContainer}>
      <div className={styles.dropdown} onClick={toggleDropdown}>
        <span>{selectedSize}</span>
        <Image src={Arrow} alt='' className={styles.svgImg}/>
      </div>

      {showDropdown && (
        <ul className={styles.dropdownList}>
          {props.values.map((value) => (
            <li key={value} onClick={() => handleSizeSelection(value)} className={styles.dropdownItem}>
              {value}
            </li>
          ))}
        </ul>
      )}
    </div>
  );
}

export default TamanhoDropdown;
