"use client"

import Link from "next/link"
import styles from "./header.module.css"
import Image from "next/image"
import { useWindowSize } from "@/app/hooks/useWindowSize"
import { useState } from "react"
import { Overlay } from "../Overlay/Overlay"
import { useRouter } from "next/navigation"

export function Header() {

    const { width } = useWindowSize();

    const [productsDrowdown, setProductsDropdown] = useState<boolean>(false);
    const [collectionsDrowdown, setCollectionsDropdown] = useState<boolean>(false);

    const [burguerMenu, setBurguerMenu] = useState<boolean>(false);

    const router = useRouter();

    return (
        <header className={styles.header}>
            <a className={styles.logo} href="/">
                BLVCK
            </a>

            {width >= 950 ? (
                <nav className={styles.menu}>

                    <div className={styles.dropdownArea}>
                        <a className={styles.link} onClick={() => setProductsDropdown(!productsDrowdown)}>Produtos</a>
                        {productsDrowdown ? (
                        <Overlay setOpen={setProductsDropdown}>
                            <div className={styles.dropdown}>
                                <a href="/produtos">Camisas</a>
                                <a href="/produtos">Calças</a>
                                <a href="/produtos">Casacos</a>
                                <a href="/produtos">Acessórios</a>
                                <a href="/produtos">Masculino</a>
                                <a href="/produtos">Feminino</a>
                            </div>
                        </Overlay>
                        ) : null} 
                    </div>

                    <div className={styles.dropdownArea}>
                        <a className={styles.link} onClick={() => setCollectionsDropdown(!collectionsDrowdown)}>Coleções</a>

                        {collectionsDrowdown ? (
                        <Overlay setOpen={setCollectionsDropdown}>
                            <div className={styles.dropdown}>
                                <a href="/produtos">Keith Haring & Blvck</a>
                                <a href="/produtos">Fortnite & Blvck</a>
                                <a href="/produtos">Shorts</a>
                                <a href="/produtos">White</a>
                            </div>
                        </Overlay>
                        ) : null} 
                    </div>

                    <Link className={styles.link} href={"/"}>Sobre</Link>
                </nav>
            ) : null}
                
            <div className={styles.userInfo}>
                <Image
                    className={styles.icon} 
                    src="/images/favorite.svg"
                    alt="favorite icon"
                    width={24}
                    height={24}
                />
                <Image
                    className={styles.icon} 
                    src="/images/cart.svg" 
                    alt="cart icon"
                    width={24}
                    height={24}
                    onClick={() => router.push("/carrinho")}
                />
                <Image
                    className={styles.icon} 
                    src="/images/user.svg" 
                    alt="user icon"
                    width={24}
                    height={24}
                />
                {width < 950 ? (
                    <div className={styles.burguerMenuArea}>
                        <Image
                            className={styles.icon} 
                            src="/images/menu.svg" 
                            alt="menu icon"
                            width={24}
                            height={24}
                            onClick={() => setBurguerMenu(!burguerMenu)}
                        />
                        {burguerMenu ? (
                            <Overlay setOpen={setBurguerMenu}>
                                <div className={styles.burguerMenu}>
                                    <section>
                                        <p>Produtos</p>

                                        <div className={styles.linkArea}>
                                            <a href="/produtos">Camisas</a>
                                            <a href="/produtos">Calças</a>
                                            <a href="/produtos">Casacos</a>
                                            <a href="/produtos">Acessórios</a>
                                            <a href="/produtos">Masculino</a>
                                            <a href="/produtos">Feminino</a>
                                        </div>
                                    </section>

                                    <section>
                                        <p>Coleções</p>

                                        <div className={styles.linkArea}>
                                            <a href="/produtos">Keith Haring & Blvck</a>
                                            <a href="/produtos">Fortnite & Blvck</a>
                                            <a href="/produtos">Shorts</a>
                                            <a href="/produtos">White</a>
                                        </div>
                                    </section>
                                </div>
                            </Overlay>
                        ) : null}
                        
                    </div>

                ) : null}
                
            </div>
        </header>
    )
}