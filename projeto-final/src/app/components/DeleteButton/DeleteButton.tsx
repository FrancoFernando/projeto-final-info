import React, { useState, useEffect, useRef } from 'react';
import styles from './DeleteButton.module.css';

interface DeleteButtonPropsType {
  onDelete: () => void;
}

export default function DeleteButton(props: DeleteButtonPropsType) {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const buttonRef = useRef(null);

  const handleDelete = () => {
    props.onDelete(); // Invocando a função onDelete
    setIsModalOpen(false);
  };

  const handleClickOutside = (event: MouseEvent) => {
    const modal = document.querySelector(`.${styles.Modal}`);
    const deleteButton = buttonRef.current;

    if (modal && !modal.contains(event.target as Node) && event.target !== deleteButton) {
      setIsModalOpen(false);
    }
  };

  useEffect(() => {
    if (isModalOpen) {
      document.addEventListener('mousedown', handleClickOutside);
    }

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [isModalOpen]);

  return (
    <>
      <button
        ref={buttonRef}
        className={styles.DeleteButton}
        onClick={() => setIsModalOpen(true)}
      >
        Excluir
      </button>

      {isModalOpen && (
        <div className={styles.Modal}>
          <div className={styles.ModalContent}>
            <p>Tem certeza que deseja excluir esse produto?</p>
            <div className={styles.ModalButtonsContainer}>
              <button className={styles.AcceptButton} onClick={handleDelete}>
                Sim
              </button>
              <button
                className={styles.DeclineButton}
                onClick={() => setIsModalOpen(false)}
              >
                Não
              </button>
            </div>
          </div>
        </div>
      )}
    </>
  );
}
