"use client"

import Image from "next/image"
import styles from "./footer.module.css"
import { useWindowSize } from "@/app/hooks/useWindowSize"

export function Footer() {

    const { width } = useWindowSize();

    return (
        <footer className={styles.footer}>
            <div className={styles.newsLetter}>
                <p>Participe da nossa Newsletter e fique por dentro dos lançamentos</p>
                <div className={styles.inputArea}>
                    <input className={styles.input} type="text"  placeholder="Digite o seu email"/>
                    <button className={styles.inputButton}>
                        <Image
                            alt="right arrow icon"
                            src="/images/rightArrow.svg"
                            width={20}
                            height={20}
                        />
                    </button>
                </div>

                {
                    width >= 900 ? (
                        <div className={styles.logo}>
                            BLVCK
                        </div>
                    ) : null
                }
                
            </div>
            <div className={styles.additionalInfo}>
                <a href="#">FAQ</a>
                <a href="#">Termos e condições</a>
                <a href="#">contato@blvck.com</a>
                <div className={styles.socialMedias}>
                    <Image
                        className={styles.socialMedia}
                        alt="right arrow icon"
                        src="/images/facebook.svg"
                        width={32}
                        height={32}
                    />
                    <Image
                        className={styles.socialMedia}
                        alt="right arrow icon"
                        src="/images/instagram.svg"
                        width={32}
                        height={32}
                    />
                    <Image
                        className={styles.socialMedia}
                        alt="right arrow icon"
                        src="/images/linkedin.svg"
                        width={32}
                        height={32}
                    />
                    <Image
                        className={styles.socialMedia}
                        alt="right arrow icon"
                        src="/images/x.svg"
                        width={32}
                        height={32}
                    />
                </div>
            </div>

            {
                    width < 900 ? (
                        <div className={styles.logo}>
                            BLVCK
                        </div>
                    ) : null
                }
        </footer>
    )
}