'use client'
import style from './Modal.module.css';
import { Input } from '../Input/Input';
import { useState } from 'react';
import file_upload from '../../images/file_upload.svg';
import Delete from '../../images/delete.svg';
import add from '../../images/add.svg';
import Image from 'next/image';
import { useWindowSize } from "@/app/hooks/useWindowSize"
import { Product } from "@/app/models/Product"


interface ModalProps {
    isOpen: boolean;
    title: string;
    product: Product | undefined;
    close: () => void;
    update: () => void;
}

function allowedNum( s:string, int : boolean = true ){
    const lst = s.slice(-1);
    
    if( lst >= "0" && lst <= "9")
        return true;
    
    if( lst == "." && s.split(".").length < 3 && !int && !s.includes(",") )
        return true;

    if( lst == "," && s.split(",").length < 3 && !int && !s.includes(".") )
        return true;

    return false;    
}

function swap( num : number, image : string[], setImage : (image : string[]) => void){
    if(image.length <= num)
        return;
    
    let ar = [...image];

    let aux = ar[0];
    ar[0] = ar[num];
    ar[num] = aux;
    setImage(ar);
}

function handleFile(e: any, image : string[], setImage : (image : string[]) => void) {
    const files = e.target.files; 

    if(files)
        for(let i = 0 ; i < files.length ; i++){
            const reader = new FileReader();
            reader.onload = function(evt) { 
                let contents;
                if(evt){
                    contents = evt.target?.result;
                    if(contents)
                        setImage([...image, contents as string])
                }
            };
            reader.readAsDataURL(files[i]);
        }
            
}

function handleDelete(num : number, setShow : (num : number) => void, image : string[], setImage : (image : string[]) => void){
    if(image.length <= num)
        return;
    
    let ar = [...image];
    ar.splice(num,1);
    setImage(ar);
    if(num > 0)
        setShow(num - 1);
    else if(num == 0 && ar.length > 0)
        setShow(0);
    else
        setShow(-1);
}

export function Modal ({ isOpen, title, product, close, update } : ModalProps ){
    const { width } = useWindowSize()

    const [name, setName] = useState(product ? product.name : "");
    const [price, setPrice] = useState(product ? product.actualPrice.toString() : "");
    const [oldPrice, setOldPrice] = useState(product ? product.previousPrice ? product.previousPrice.toString() : "" : "");
    const [p, setP] = useState(product ? product.quantity_p.toString() : "");
    const [m, setM] = useState(product ? product.quantity_m.toString() : "");
    const [g, setG] = useState(product ? product.quantity_g.toString() : "");
    const [gg, setGg] = useState(product ? product.quantity_gg.toString() : "");
    const [xgg, setXgg] = useState(product ? product.quantity_xgg.toString() : "");
    const [description, setDescription] = useState(product ? product.description : "");
    const [tags, setTags] = useState(product ? product.tags : [""]);
    const [collections, setCollections] = useState(product ? product.collections : [""]);
    const [image, setImage] = useState(product ? product.images : []);
    const [show, setShow] = useState(0);
  

    if(!isOpen) 
        return null;
    
    return (
        <div className={style.modal}>
            <div className={style.modalOverlay} onClick={() =>{ 
                    close(); 
                }}>
            </div>
            <div className={style.modalContent}>
                <div className={style.pq}>
                    <h3 className={style.title}>{title}</h3>
                    <div className={style.center}>
                        <div className={style.left}>

                            <div className={style.imgs}>
                                <div className={style.sideImagesDiv}>
                                    { image.map( (e,i) => (
                                                <div className={style.sideImagesDivDiv} key={i} onClick={ () => { setShow(i) } }>
                                                    <img src={e} alt="" />
                                                </div>
                                            )
                                        )
                                    }

                                    <label htmlFor="imgupload"><Image src={add} alt="add"/></label>
                                    
                                </div>

                                <div className={style.mainImageDiv}>
                                    <div className={style.mainImageDivDiv}>
                                        {image[show] ? <img src={image[show]}/> : null}
                                    </div>
                                    
                                    <div className={`${style.delete} ${style.overImg}`} onClick={() => handleDelete(show, setShow, image, setImage)}>
                                        <Image src={Delete} alt="Delete"/>
                                    </div>
                                    
                                    <div className={`${style.file_upload} ${style.overImg}`}>
                                        <input onChange={ (e) => {handleFile(e, image, setImage)}} type="file" id="imgupload" name="avatar" accept="image/png, image/jpeg" style={{display: "none"}}/>
                                        <label htmlFor="imgupload"><Image src={file_upload} alt="file_upload"/></label>
                                        
                                    </div>
                                
                                </div>
                            </div>

                            <div className={style.buttons}>
                                <button className={style.black} onClick={() =>{
                                    
                                    if(product) {
                                        product.name = name;
                                        product.actualPrice = parseFloat(price);
                                        product.previousPrice = parseFloat(oldPrice);
                                        product.quantity_p = parseInt(p);
                                        product.quantity_m = parseInt(m);
                                        product.quantity_g = parseInt(g);
                                        product.quantity_gg = parseInt(gg);
                                        product.quantity_xgg = parseInt(xgg);
                                        product.description = description;
                                        product.tags = tags;
                                        product.collections = collections;
                                        product.quantity = parseInt(p) + parseInt(m) + parseInt(g) + parseInt(gg) + parseInt(xgg);
                                        product.images = [""];
                                    }
                                    else
                                        product = {
                                            id: -1,
                                            name: name,
                                            actualPrice: parseFloat(price),
                                            previousPrice: parseFloat(oldPrice),
                                            quantity_p: parseInt(p),
                                            quantity_m: parseInt(m),
                                            quantity_g: parseInt(g),
                                            quantity_gg: parseInt(gg),
                                            quantity_xgg: parseInt(xgg),
                                            description: description,
                                            tags: tags,
                                            collections: collections,
                                            quantity: parseInt(p) + parseInt(m) + parseInt(g) + parseInt(gg) + parseInt(xgg),
                                            images: [""],
                                        }
                                    close();
                                    update();
                                    }}
                                >
                                    Salvar</button>
                                <button className={style.white}>Excluir</button>
                                <button className={style.white} onClick={close}>Cancelar</button>
                            </div>

                        </div>
                        <div className={style.right}>

                            <div className={style.block}>
                                <p>Nome</p>
                                <Input placeholder="Nome do Produto" value={name} onChange={(e) => setName(e)} width={290}/>
                            </div>

                            <div className={style.row}>
                                <div className={style.block}>
                                    <p>Preço Atual</p>
                                    <Input 
                                        placeholder="333.00" 
                                        value={price} 
                                        onChange={(e) => {
                                            e = e != "" ? e : "0";
                                            if(allowedNum(e, false)){
                                                if( price == "0" && e.slice(-1) != "." && e.slice(-1) != ",")
                                                    setPrice(parseFloat(e).toString());
                                                else
                                                    setPrice(e);

                                                if(product)
                                                    product.actualPrice = parseFloat(e);
                                                
                                            }
                                        }} 
                                        symbol="R$"
                                    />
                                </div>

                                <div className={style.block}>
                                    <p>Preço Antigo <span>(opcional)</span></p>
                                    <Input 
                                        placeholder="333.00" 
                                        value={oldPrice} 
                                        onChange={(e) => {
                                            e = e != "" ? e : "0";
                                            if(allowedNum(e, false)){
                                                if( oldPrice == "0"  && e.slice(-1) != "." && e.slice(-1) != ",")
                                                    setOldPrice(parseFloat(e).toString());
                                                else
                                                    setOldPrice(e);
                                                product ? product.previousPrice = parseFloat(e) : null;
                                            }
                                        }} 
                                        symbol="R$"
                                    />
                                </div>
                            </div>

                            <div className={style.block}>
                                <p>Quantidade</p>
                                <div className={style.row} >
                                    <Input 
                                        placeholder="0" 
                                        value={p} 
                                        onChange={(e) => {
                                            e = e != "" ? e : "0";
                                            if(allowedNum(e)){
                                                if( p == "0")
                                                    setP(parseInt(e).toString());
                                                else
                                                    setP(e);
                                            }
                                        }} 
                                        width={width < 1000 ? 30 : 48} 
                                        symbol="P"
                                    />
                                    <Input 
                                        placeholder="0" 
                                        value={m} 
                                        onChange={(e) => {
                                            e = e != "" ? e : "0";
                                            if(allowedNum(e)){
                                                if( m == "0")
                                                    setM(parseInt(e).toString());
                                                else
                                                    setM(e);
                                            }
                                        }} 
                                        width={width < 1000 ? 30 : 48} 
                                        symbol="M"
                                    />
                                    <Input 
                                        placeholder="0" 
                                        value={g} 
                                        onChange={(e) => {
                                            e = e != "" ? e : "0";
                                            if(allowedNum(e)){
                                                if( g == "0")
                                                    setG(parseInt(e).toString());
                                                else
                                                    setG(e);
                                            }
                                        }} 
                                        width={width < 1000 ? 30 : 48} 
                                        symbol="G"
                                    />
                                    <Input 
                                        placeholder="0" 
                                        value={gg} 
                                        onChange={(e) => {
                                            e = e != "" ? e : "0";
                                            if(allowedNum(e)){
                                                if( p == "0")
                                                    setGg(parseInt(e).toString());
                                                else
                                                    setGg(e);
                                            }
                                        }} 
                                        width={width < 1000 ? 30 : 48} 
                                        symbol="GG"
                                    />
                                    <Input 
                                        placeholder="0" 
                                        value={xgg} 
                                        onChange={(e) => {
                                            e = e != "" ? e : "0";
                                            if(allowedNum(e)){
                                                if( xgg == "0")
                                                    setXgg(parseInt(e).toString());
                                                else
                                                    setXgg(e);
                                            }
                                        }} 
                                        width={width < 1000 ? 30 : 48} 
                                        symbol="XGG"
                                    />
                                </div>
                            </div>

                            <div className={style.block}>
                                <p>Descrição do Produto</p>
                                <Input placeholder="Descrição do Produto" value={description} onChange={(e) => {setDescription(e)}} width={width < 1430 ? 313 : 550} type="textarea"/>
                            </div>

                            <div className={style.block}>
                                <p>Tags</p>
                                <Input placeholder="Digite a tag" onChange={(e) => { tags? setTags([...tags, e]) : setTags([""])}} width={width < 1000 ? 313 : 290}/>
                                { tags?.map( (e,i)=> (<div key={i}>{e}</div>) ) }
                            </div>

                            <div className={style.block}>
                                <p>Coleção</p>
                                <Input 
                                    type="dropdown" 
                                    value={"default"} 
                                    placeholder="Selecione a coleção" 
                                    onChange={(e) => { collections? setCollections([...collections, e]) : setCollections([""])}} 
                                    options={ collections } 
                                    width={136}
                                />
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div className={style.phoneButton}>
                <button className={style.blackPhone}>Salvar</button>
                <button className={style.whitePhone}>Excluir</button>
                <button className={style.whitePhone} onClick={close}>Cancelar</button> 
            </div>
        </div>
    )
}