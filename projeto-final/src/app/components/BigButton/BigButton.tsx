'use client';

import React from 'react';
import styles from './BigButton.module.css';

interface BigButtonPropsType {
  buttonContent: string;
  color: 'black' | 'white';
  shadow: boolean;
  enable: boolean;
  onClick?: () => void;
}

export default function BigButton(BigButtonProps: BigButtonPropsType) {
  const buttonClasses = `${styles.BigButton} ${BigButtonProps.color === 'black' ? styles.BlackBigButton : styles.WhiteBigButton}`;
  const buttonStyles = {
    boxShadow: BigButtonProps.shadow ? '8px 8px 0px 0px #BDBDBD' : 'none',
    cursor: BigButtonProps.enable ? 'pointer' : 'not-allowed', // Altera o cursor com base na propriedade enable
  };

  // Cria um objeto separado para propriedades condicionais
  const conditionalStyles: React.CSSProperties = {};

  if (!BigButtonProps.enable) {
    conditionalStyles['pointerEvents'] = 'none';
    conditionalStyles['opacity'] = '0.5';
  }

  // Mescla os objetos buttonStyles e conditionalStyles
  const mergedStyles = { ...buttonStyles, ...conditionalStyles };

  return (
    <>
      <button
      className={buttonClasses}
      style={mergedStyles}
      disabled={!BigButtonProps.enable}
      onClick={BigButtonProps.onClick}
      >
        {BigButtonProps.buttonContent}
      </button>
    </>
  );
}
