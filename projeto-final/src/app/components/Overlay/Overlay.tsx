import { ReactNode, useEffect, useRef } from "react"
import styles from "./overlay.module.css"

type OverlayProps = {
    children: ReactNode,
    setOpen: (value: boolean) => void,
}


export function Overlay({children, setOpen}: OverlayProps) {

  const modalRef = useRef<HTMLDivElement>(null);

  const handleClickOutside = (event: MouseEvent) => {
    setTimeout(()=>{
      if (modalRef.current && !modalRef.current.contains(event.target as Node)) {
        setOpen(false);
      }
    }, 120)
  };

  useEffect(() => {
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, []);

  return <div ref={modalRef}>{children}</div>;
}