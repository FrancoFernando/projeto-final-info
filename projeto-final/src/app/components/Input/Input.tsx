import styles from './Input.module.css';


interface InputProps {
    placeholder: string;
    onChange: (arg0: string) => void;
    width?: number;
    value?: string;
    symbol?: string;
    type?: string;
    options?: string[];
}

export function Input( { placeholder, onChange, width, value, symbol, type, options} : InputProps ){

    let s = null;

    if(symbol)
        s = (<div className={styles.Symbol}> {symbol} </div>);

    let input = (<div className={styles.group}>
                    {s}
                    <input type="text" value={value} placeholder={placeholder} className={styles.inputA} onChange={ event => onChange(event.target.value) } style={{width: (width ? width : "80") +"px"}}/>
                </div>);

    if(type == "dropdown" && options)
        input = ( <div className={styles.selectBox}>
                        <select onChange={ event => onChange(event.target.value) }  style={{width: (width ? width : "80") +"px"}}>
                            {options.map( (option, index) => (<option key={index}>{option}</option>))}
                        </select>
                    </div>
                );
    else if( type == "textarea")
        input = (<textarea className={styles.textarea} onChange={ event => onChange(event.target.value) } rows={6} value={value} name = "descrição" placeholder={placeholder} style={{width: (width ? width : "80") +"px"}}></textarea>);

    return(
        <div className={styles.input}>
            {input}
        </div>
    ); 
}