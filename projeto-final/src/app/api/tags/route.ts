import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";


const prisma = new PrismaClient();

export async function GET(request: Request) {
    try {
        const tags = await prisma.tag.findMany();

        return NextResponse.json({
            success: true,
            tags,
        });

    } catch (error: any) {
        return NextResponse.json({
            success: false,
            error: error.message,
        });
    }
}
