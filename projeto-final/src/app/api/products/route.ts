import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";


const prisma = new PrismaClient();

export async function POST(request: Request) {
    try {
        const {
            name,
            actualPrice,
            previousPrice,
            quantity_p,
            quantity_m,
            quantity_g,
            quantity_gg,
            quantity_xgg,
            description,
            collectionId,
            images,
            tags,
        } = await request.json();


        if (!Array.isArray(tags) || tags.some((tag: any) => !tag.name || typeof tag.name !== 'string')) {
            return NextResponse.json({
                success: false,
                error: "Invalid tags",
            });
        }

        // Check if the collection exists
        const existingCollection = await prisma.collection.findUnique({
            where: {
                id: collectionId,
            },
        });

        if (!existingCollection) {
            return NextResponse.json({
                success: false,
                error: "Collection does not exist",
            });
        }

        // Create the product with images
        const product = await prisma.product.create({
            data: {
                name,
                actualPrice,
                previousPrice,
                quantity_p,
                quantity_m,
                quantity_g,
                quantity_gg,
                quantity_xgg,
                description,
                collection: {
                    connect: { id: collectionId },
                },
                images,
                tags: {
                    create: tags.map((tag: any) => ({
                        tag: {
                            create: {
                                name: tag.name,
                            },
                        },
                    })),
                },
            },
            include: {
                collection: true,
                tags: true,
            },
        });

        product.images = JSON.parse(product.images);

        return NextResponse.json({
            success: true,
            product,
        });
    } catch (error: any) {
        return NextResponse.json({
            success: false,
            error: error.message,
        });
    }
}

export async function DELETE(request: Request) {
    try {
        const { productId } = await request.json();

        await prisma.product.delete({
            where: {
                id: productId,
            },
        });

        return NextResponse.json({
            success: true,
        });

    } catch (error: any) {
        return NextResponse.json({
            success: false,
            error: error.message,
        });
    }
}

export async function PUT(request: Request) {
    try {
        const { productId, ...updatedFields } = await request.json();

        // Verificar se o produto existe
        const existingProduct = await prisma.product.findUnique({
            where: {
                id: productId,
            },
        });

        if (!existingProduct) {
            return NextResponse.json({
                success: false,
                error: "Product not found",
            });
        }

        // Atualizar o produto
        const updatedProduct = await prisma.product.update({
            where: {
                id: productId,
            },
            data: updatedFields,
            include: {
                collection: true,
                tags: true,
            },
        });

        updatedProduct.images = JSON.parse(updatedProduct.images);

        return NextResponse.json({
            success: true,
            product: updatedProduct,
        });
    } catch (error: any) {
        return NextResponse.json({
            success: false,
            error: error.message,
        });
    }
}


export async function GET(request: Request) {
    try {
        const { searchParams } = new URL(request.url);

        const tagsString = searchParams.getAll("tags")[0] as string | undefined;
        const tags = tagsString ? tagsString.split(",") : undefined;

        const collectionsString = searchParams.getAll("collections")[0] as string | undefined;
        const collections = collectionsString ? collectionsString.split(",") : undefined;

        let includeClause = {
            collection: true,
            tags: true,
        };

        let whereClause = {};

        if (tags && tags.length > 0) {
            whereClause = {
                ...whereClause,
                tags: {
                    some: {
                        tag: {
                            name: {
                                in: tags,
                            },
                        },
                    },
                },
            };
        }

        if (collections && collections.length > 0) {
            whereClause = {
                ...whereClause,
                collection: {
                    name: {
                        in: collections,
                    },
                },
            };
        }

        const products = await prisma.product.findMany({
            where: whereClause,
            include: includeClause,
        });

        return NextResponse.json({
            success: true,
            products,
        });
    } catch (error: any) {
        return NextResponse.json({
            success: false,
            error: error.message,
        });
    }
}