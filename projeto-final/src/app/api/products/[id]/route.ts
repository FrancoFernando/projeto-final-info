
import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

export async function GET(request: Request) {
    try {
        const param = request.url.split("products/")[1];
        const productId: number = Number(param);
        const product: any | null = await prisma.product.findUnique({
            where: { id: productId },
            include: {
                collection: true,
                tags: true,
            },
        });

        if (!product) {
            return NextResponse.json({
                success: false,
                error: "Product not found",
            });
        }
   
        return NextResponse.json({
            success: true,
            product: product
        });
        
    } catch (error: any) {
        return NextResponse.json({
            success: false,
            error: error.message,
        });
    }
}
