
import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

interface Collection {
    id: number;
    name: string;
};

export async function GET(request: Request) {
    try {
        const param = request.url.split("collections/")[1];
        const collectiontId: number = Number(param);
        const collection: Collection | null = await prisma.collection.findUnique({
            where: { id: collectiontId },
        });

        if (!collection) {
            return NextResponse.json({
                success: false,
                error: "Collection not found",
            });
        }
   
        return NextResponse.json({
            success: true,
            names: collection
        });
        
    } catch (error: any) {
        return NextResponse.json({
            success: false,
            error: error.message,
        });
    }

}
