import { NextResponse } from "next/server";
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

export async function POST(request: Request) {
    try {
        const { name } = await request.json();

        if(!name) {
            return NextResponse.json({
                success: false,
                error: "Name is required",
            });
        }

        const collection = await prisma.collection.create({
            data: {
                name,
            },
        });

        return NextResponse.json({
            success: true,
            collection,
        });

    } catch (error: any) {
        return NextResponse.json({
            success: false,
            error: error.message,
        });
    }
}

export async function DELETE(request: Request) {
    try {
        const { collectionId } = await request.json();

        await prisma.collection.delete({
            where: {
                id: collectionId,
            },
        });


        return NextResponse.json({
            success: true,
        });

    } catch (error: any) {
        return NextResponse.json({
            success: false,
            error: error.message,
        });
    }
}

export async function GET(request: Request) {
    try {
        const collections = await prisma.collection.findMany();

        return NextResponse.json({
            success: true,
            collections,
        });

    } catch (error: any) {
        return NextResponse.json({
            success: false,
            error: error.message,
        });
    }
}
