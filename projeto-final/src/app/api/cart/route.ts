import { NextResponse } from 'next/server';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

export async function POST(request: Request) {
    try {
        const { productId, size, quantity } = await request.json();

        const product = await prisma.product.findUnique({
            where: {
                id: productId,
            },
        });

        if (!product) {
            return NextResponse.json({
                success: false,
                error: 'Product not found',
            });
        }

        const sizesAvailable = ['p', 'm', 'g', 'gg', 'xgg'];
        const normalizedSize = size.toLowerCase();

        if (!sizesAvailable.includes(normalizedSize)) {
            return NextResponse.json({
                success: false,
                error: 'Invalid size',
            });
        }

        if (quantity <= 0 || !Number.isInteger(quantity)) {
            return NextResponse.json({
                success: false,
                error: 'Invalid quantity',
            });
        }

        const availableStock = (product as any)[`quantity_${normalizedSize}`] || 0;

        if (quantity > availableStock) {
            return NextResponse.json({
                success: false,
                error: 'Insufficient stock',
            });
        }

        const existingCartItem = await prisma.cartItem.findFirst({
            where: {
                productId,
                size: normalizedSize,
            },
        });

        if (existingCartItem) {
            // Update existing cart item with the calculated total value
            await prisma.cartItem.update({
                where: {
                    id: existingCartItem.id,
                },
                data: {
                    quantity: existingCartItem.quantity + quantity,
                    total: (existingCartItem.quantity + quantity) * product.actualPrice,
                },
            });
        } else {
            // Create new cart item with the calculated total value
            await prisma.cartItem.create({
                data: {
                    productId,
                    size: normalizedSize,
                    quantity,
                    total: quantity * product.actualPrice,
                },
            });
        }

        return NextResponse.json({
            success: true,
            message: 'Item added to the cart successfully',
        });
    } catch (error: any) {
        return NextResponse.json({
            success: false,
            error: error.message,
        });
    }
}


export async function DELETE(request: Request) {
    try {
        const { productId, size } = await request.json();

        // Find the cart item to delete
        const existingCartItem = await prisma.cartItem.findFirst({
            where: {
                productId,
                size: size.toLowerCase(),
            },
        });

        if (!existingCartItem) {
            return NextResponse.json({
                success: false,
                error: 'Item not found in the cart',
            });
        }

        // Delete the cart item
        await prisma.cartItem.delete({
            where: {
                id: existingCartItem.id,
            },
        });

        return NextResponse.json({
            success: true,
            message: 'Item removed from the cart successfully',
        });
    } catch (error: any) {
        return NextResponse.json({
            success: false,
            error: error.message,
        });
    }
}


export async function GET(request: Request) {
    try {
        const cartItems = await prisma.cartItem.findMany();

        return NextResponse.json({
            success: true,
            cartItems,
        });

    } catch (error: any) {
        return NextResponse.json({
            success: false,
            error: error.message,
        });
    }
}
